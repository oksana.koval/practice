const firstAttempt = 3;
const secondAttempt = 2;
const thirdAttempt = 1;
const maxAttempts = 3;
const numberRangeStep = 4;
const prizeMultiplier = 2;
const defaultRangeValue = 8;
const firstAttemptDefaultPrize = 100;
const secondAttemptDefaultPrize = 50;
const thirdAttemptDefaultPrize = 25;

let attempts = 3;
let totalPrize = 0;
let possiblePrize = 0;
let maxValue = 8;
let firstAttemptPrize = firstAttemptDefaultPrize;
let secondAttemptPrize = secondAttemptDefaultPrize;
let thirdAttemptPrize = thirdAttemptDefaultPrize;

function start() {
    let message = confirm('Do you want to play a game?');
    if (message) {
        startGame();
    } else {
        alert('You did not become a billionaire, but can.');
    }
}

function startGame() {
    let randomNumber = Math.floor(Math.random() * (maxValue + 1));
    while (attempts > 0) {
        switch (attempts) {
            case firstAttempt:
                possiblePrize = firstAttemptPrize;
                break;
            case secondAttempt:
                possiblePrize = secondAttemptPrize;
                break;
            case thirdAttempt:
                possiblePrize = thirdAttemptPrize;
                break;
            default:
                break;
        }
        let inputNumber = Number(prompt(`Choose a roulette pocket number from 0 to ${maxValue}
Attempts left: ${attempts}\nTotal prize: ${totalPrize}$
Possible prize on current attempt: ${possiblePrize}$`));
        if (inputNumber !== randomNumber) {
            attempts--;
            continue;
        }
        switch (attempts) {
            case firstAttempt:
                totalPrize += firstAttemptPrize;
                break;
            case secondAttempt:
                totalPrize += secondAttemptPrize;
                break;
            case thirdAttempt:
                totalPrize += thirdAttemptPrize;
                break;
            default:
                break;
        }
        let isContinue = confirm(`Congratulation, you won! Your prize is: ${totalPrize}$. Do you want to continue?`);
        if (!isContinue) {
            alert(`Thank you for your participation. Your prize is: ${totalPrize}$`);
            return;
        }
        attempts = maxAttempts;
        maxValue += numberRangeStep;
        firstAttemptPrize *= prizeMultiplier;
        secondAttemptPrize *= prizeMultiplier;
        thirdAttemptPrize *= prizeMultiplier;
        return startGame();
    }
    alert(`Thank you for your participation. Your prize is: ${totalPrize}$`);
    let isPlaying = confirm('Do you want to play a game again?');
    if (isPlaying) {
        attempts = maxAttempts;
        totalPrize = 0;
        maxValue = defaultRangeValue;
        firstAttemptPrize = firstAttemptDefaultPrize;
        secondAttemptPrize = secondAttemptDefaultPrize;
        thirdAttemptPrize = thirdAttemptDefaultPrize;
        startGame();
    }
}


start();