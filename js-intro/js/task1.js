function calculateProfit() {
    const minAmountOfMoney = 1000;
    const minNumberOfYears = 1;
    const maxPercentageOfYear = 100;
    const percents = 100;
    const digitsAfterPoint = 2;

    let amountOfMoney = prompt('Initial amount:');
    if (amountOfMoney === null) {
        return;
    }
    amountOfMoney = Number(amountOfMoney);
    let validAmount = !isNaN(amountOfMoney) && amountOfMoney >= minAmountOfMoney;
    if (!validAmount) {
        alert('Invalid input data');
        return calculateProfit();
    }

    let numberOfYears = prompt('Number of years:');
    if (numberOfYears === null) {
        return;
    }
    numberOfYears = Number(numberOfYears);
    let validYears = Number.isInteger(numberOfYears) && numberOfYears >= minNumberOfYears;
    if(!validYears) {
        alert('Invalid input data');
        return calculateProfit();
    }

    let percentageOfYear = prompt('Percentage of year:');
    if (percentageOfYear === null) {
        return;
    }
    percentageOfYear = Number(percentageOfYear);
    let validPercentage = !isNaN(percentageOfYear) && percentageOfYear <= maxPercentageOfYear && percentageOfYear > 0;
    if(!validPercentage) {
        alert('Invalid input data');
        return calculateProfit();
    }

    let totalProfit = 0;
    let totalAmount = amountOfMoney;

    for (let i = 0; i < numberOfYears; i++) {
        totalProfit += totalAmount * (percentageOfYear / percents);
        totalAmount = amountOfMoney + totalProfit;
    }

    alert(`Initial amount: ${amountOfMoney}
Number of years: ${numberOfYears}
Percentage of year: ${percentageOfYear}
        
Total profit: ${totalProfit.toFixed(digitsAfterPoint)}
Total amount: ${totalAmount.toFixed(digitsAfterPoint)}`
    );
}

calculateProfit();