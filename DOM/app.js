const appRoot = document.getElementById('app-root');

let isSortByCountry = true;
let isSortAscending = false;

let header = document.createElement('header');
let title = document.createTextNode('Countries Search');
let h1 = document.createElement('h1');
let form = document.createElement('form');
let div = document.createElement('div');
let label = document.createElement('label');
let typeFieldDiv = document.createElement('div');
let radioDiv = document.createElement('div');
let queryFieldDiv = document.createElement('div');
let regionInput = document.createElement('input');
let languageInput = document.createElement('input');
let regionLabel = document.createElement('label');
let textForRegion = document.createTextNode('By Region');
let languageLabel = document.createElement('label');
let textForLanguage = document.createTextNode('By Language');
let pForTypeField = document.createElement('p');
let textForType = document.createTextNode('Please choose the type of search:');
let labelForQueryField = document.createElement('label');
let textForQuery = document.createTextNode('Please choose search query:');
let selectForQuery = document.createElement('select');
let option = document.createElement('option');
let textForOption = document.createTextNode('Select value');
let upDownArrowHTMLCode = '&#8645;';
let upArrowHTMLCode = '&#8593;';
let downArrowHTMLCode = '&#8595;';

let pForNoItem = document.createElement('p');
pForNoItem.className = 'noItem';

let textForNoItemP = document.createTextNode('No items, please choose search query');

appRoot.appendChild(header);
header.appendChild(h1);
h1.appendChild(title);

appRoot.appendChild(form);
typeFieldDiv.setAttribute('class', 'type-field');
form.appendChild(typeFieldDiv);
typeFieldDiv.appendChild(pForTypeField);
pForTypeField.appendChild(textForType);

radioDiv.setAttribute('class', 'radio-div')

regionInput.setAttribute('type', 'radio');
regionInput.setAttribute('id', 'region');
regionInput.setAttribute('name', 'type');
regionInput.setAttribute('value', 'region');
regionInput.setAttribute('onchange', 'createList()')
radioDiv.appendChild(regionInput);
typeFieldDiv.appendChild(radioDiv);

regionLabel.setAttribute('for', 'region');
regionLabel.appendChild(textForRegion);
radioDiv.appendChild(regionLabel);
typeFieldDiv.appendChild(radioDiv);

languageInput.setAttribute('type', 'radio');
languageInput.setAttribute('id', 'language');
languageInput.setAttribute('name', 'type');
languageInput.setAttribute('value', 'language');
languageInput.setAttribute('onchange', 'createList()');
radioDiv.appendChild(languageInput);
typeFieldDiv.appendChild(radioDiv);

languageLabel.setAttribute('for', 'language');
languageLabel.appendChild(textForLanguage);
radioDiv.appendChild(languageLabel);
typeFieldDiv.appendChild(radioDiv);

form.appendChild(queryFieldDiv);
queryFieldDiv.appendChild(labelForQueryField);
queryFieldDiv.setAttribute('class', 'query-field')
labelForQueryField.setAttribute('for', 'query-select')
labelForQueryField.appendChild(textForQuery);
selectForQuery.setAttribute('name', 'query');
selectForQuery.setAttribute('id', 'query-select');
selectForQuery.disabled = true;
queryFieldDiv.appendChild(selectForQuery);
selectForQuery.appendChild(option);
option.appendChild(textForOption);

function createList() {
    if ((document.getElementById('region').checked
            || document.getElementById('language').checked)
        && selectForQuery.selectedIndex === 0) {
        pForNoItem.appendChild(textForNoItemP);
        appRoot.appendChild(pForNoItem);
    }

    if (selectForQuery.disabled) {
        selectForQuery.disabled = false;
    }
    selectForQuery.querySelectorAll('*').forEach(el => el.remove());
    selectForQuery.appendChild(option);
    if (document.getElementById('region').checked) {
        let regionArray = externalService.getRegionsList();
        for (let i = 0; i < regionArray.length; i++) {
            let option = document.createElement('option');
            option.value = regionArray[i];
            option.text = regionArray[i];
            selectForQuery.appendChild(option);
        }
    } else if (document.getElementById('language').checked) {
        let languageArray = externalService.getLanguagesList();
        for (let i = 0; i < languageArray.length; i++) {
            let option = document.createElement('option');
            option.value = languageArray[i];
            option.text = languageArray[i];
            selectForQuery.appendChild(option);
        }
    }
}

function generateTable(query) {
    let table = document.createElement('table');
    table.setAttribute('id', 'table');
    table.setAttribute('class', 'app-table');
    table.setAttribute('border', '2');

    if (appRoot.getElementsByClassName('noItem').length !== 0) {
        appRoot.removeChild(pForNoItem);
    }

    let currentTable = appRoot.getElementsByClassName('app-table');
    if (currentTable.length !== 0) {
        currentTable[0].remove();
    }

    let data;
    let headers = ['Country', 'Capital', 'World region', 'Languages', 'Area', 'Flag'];

    let isRegionType = document.getElementById('region').checked;

    data = isRegionType
        ? externalService.getCountryListByRegion(query)
        : externalService.getCountryListByLanguage(query);

    let headerRow = document.createElement('thead');
    headers.forEach(headerText => {
        let header = document.createElement('th');
        let textNode = document.createTextNode(headerText);
        header.appendChild(textNode);
        header.setAttribute('class', `header-${headerText}`)
        headerRow.appendChild(header);
        table.appendChild(headerRow);
    });

    let tbody = document.createElement('tbody');
    table.appendChild(tbody);

    data.forEach(el => {
        let tr = document.createElement('tr')
        let country = document.createTextNode(el.name);
        let countryTd = document.createElement('td');
        countryTd.appendChild(country);
        tr.appendChild(countryTd);

        let capital = document.createTextNode(el.capital);
        let capitalTd = document.createElement('td');
        capitalTd.appendChild(capital);
        tr.appendChild(capitalTd);

        let worldRegion = document.createTextNode(el.region);
        let worldRegionTd = document.createElement('td');
        worldRegionTd.appendChild(worldRegion);
        tr.appendChild(worldRegionTd);

        let languageStr = Object.values(el.languages).join(', ');
        let languages = document.createTextNode(languageStr);
        let languagesTd = document.createElement('td');
        languagesTd.appendChild(languages);
        tr.appendChild(languagesTd);

        let area = document.createTextNode(el.area);
        let areaTd = document.createElement('td');
        areaTd.appendChild(area);
        tr.appendChild(areaTd);

        let flagTd = document.createElement('td');
        flagTd.innerHTML = `<img alt="flag" src=${el.flagURL}>`
        tr.appendChild(flagTd);
        tbody.appendChild(tr);
    })
    appRoot.appendChild(table);

    let headerCountrySorting = document.querySelector('.header-Country');
    headerCountrySorting.innerHTML += upDownArrowHTMLCode;
    headerCountrySorting.addEventListener('click', sortByCountry);

    let headerAreaSorting = document.querySelector('.header-Area');
    headerAreaSorting.innerHTML += upDownArrowHTMLCode;
    headerAreaSorting.addEventListener('click', sortByArea);

    sortByCountry();
}

function sortByCountry() {
    let countryField = document.querySelector('.header-Country');
    let areaField = document.querySelector('.header-Area');

    if (isSortByCountry) {
        if (isSortAscending) {
            isSortAscending = false;
            countryField.innerHTML = `Country ${downArrowHTMLCode}`;
        } else {
            isSortAscending = true;
            countryField.innerHTML = `Country ${upArrowHTMLCode}`;
        }
    } else {
        isSortByCountry = true;
        isSortAscending = true;
        countryField.innerHTML = `Country ${upArrowHTMLCode}`;
        areaField.innerHTML = `Area ${upDownArrowHTMLCode}`;
    }
    let table = document.getElementById('table');
    sortTable(table, 0, !isSortAscending);
}

function sortByArea() {
    let countryField = document.querySelector('.header-Country');
    let areaField = document.querySelector('.header-Area');

    if (!isSortByCountry) {
        if (isSortAscending) {
            isSortAscending = false;
            areaField.innerHTML = `Area ${downArrowHTMLCode}`;
        } else {
            isSortAscending = true;
            areaField.innerHTML = `Area ${upArrowHTMLCode}`;
        }
    } else {
        isSortByCountry = false;
        isSortAscending = true;
        countryField.innerHTML = `Country ${upDownArrowHTMLCode}`;
        areaField.innerHTML = `Area ${upArrowHTMLCode}`;
    }
    let table = document.getElementById('table');
    sortTable(table, 4, !isSortAscending);
}

function sortTable(table, col, reverse) {
    let tb = table.tBodies[0];
    let tr = Array.prototype.slice.call(tb.rows, 0);
    let i;
    reverse = -(+reverse || -1);
    tr = tr.sort(function (a, b) {
        return reverse
            * a.cells[col].textContent.trim()
                    .localeCompare(b.cells[col].textContent.trim(), undefined, {numeric: true})
            ;
    });
    for (i = 0; i < tr.length; ++i) {
        tb.appendChild(tr[i]);
    }
}


selectForQuery.setAttribute('onchange', 'generateTable(this.options[this.selectedIndex].value)');

