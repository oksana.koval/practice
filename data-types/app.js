function reverseNumber(num) {
    let reverseNum = 0;
    let lastDigit;
    let number = Math.abs(num);
    let divisor = 10;
    let multiplicand = 10;
    let remainder = 10;

    while (number !== 0) {
        lastDigit = number % remainder;
        reverseNum = reverseNum * multiplicand + lastDigit;
        number = Math.floor(number / divisor);
    }
    if (num < 0) {
        return -Math.abs(reverseNum);
    }
    return reverseNum;
}

function forEach(arr, func) {
    for (let obj of arr) {
        func(obj);
    }
}

function map(arr, func) {
    let newArray = [];
    for (let obj of arr) {
        newArray.push(func(obj));
    }
    return newArray;
}

function filter(arr, func) {
    let newArray = [];
    for (let obj of arr) {
        if (func(obj)) {
            newArray.push(obj);
        }
    }
    return newArray;
}

function getAdultAppleLovers(data) {
    let minAge = 18;

    let filteredArray = filter(data, el => el.age > minAge && el.favoriteFruit === 'apple');
    let getName = map(filteredArray, el => el.name);
    return getName;
}

function getKeys(obj) {
    let keysArray = [];
    for (let key in obj) {
        if (key) {
            keysArray.push(key);
        }
    }
    return keysArray;
}

function getValues(obj) {
    let valuesArray = [];
    for (let key in obj) {
        if (key) {
            valuesArray.push(obj[key]);
        }
    }
    return valuesArray;
}

function showFormattedDate(dateObj) {
    let str = dateObj.toString();
    let buffer = '';
    let separator = ' ';
    let arr = [];

    let monthIndex = 1;
    let dayIndex = 2;
    let yearIndex = 3;

    for (let c of str) {
        if (c === separator) {
            arr.push(buffer);
            buffer = '';
            continue;
        }
        buffer += c;
    }
    arr.push(buffer);
    return `It is ${arr[dayIndex]} of ${arr[monthIndex]}, ${arr[yearIndex]}`;
}