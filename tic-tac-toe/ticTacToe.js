let readline = require('readline-sync');

function renderField(field) {
    let output = '';
    let index = 0;
    let sign;
    for (let i = 1; i <= 3; i++) { // x - h
        for (let j = 1; j <= 3; j++) { // y - v
            switch (field[index]) {
                case 0:
                    sign = ' ';
                    break;
                case 1:
                    sign = 'x';
                    break;
                case -1:
                    sign = '0';
                    break;
            }
            if (j === 2) {
                output += `| ${sign} |`;
            } else {
                output += ` ${sign} `;
            }
            index += 1;
        }
        output += i !== 3 ? '\n---+---+---\n' : '';
    }
    return output;
}

function gameStatus(field) {
    const winningConditions = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];

    for (let comb of winningConditions) {
        let sum = comb.map(i => field[i]).reduce((a, c) => a + c, 0);
        switch(sum) {
            case 3: return 'x';
            case -3: return '0';
            default: continue;
        }
    }

    return field.includes(0) ? 'turn' : 'end';
}

function makeTurn(field, player, index) {
    for (let i = 0; i < field.length; i++) {
        if (i === index && field[index] === 0) {
            field[index] = player;
            return true;
        }
    }
    return false;
}

function game() {
    let field = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    let playerX = 1;
    let player0 = -1;

    let currentPlayer = 1;
    let status = gameStatus(field);

    while (status === 'turn') {
        console.log('Player 1 => [x] - Player 2 => [0]');
        console.log(renderField(field));
        let choice = readline.question(`Player ${currentPlayer % 2 ? 1 : 2}, enter your choice: `);
        if (typeof (+choice) !== 'number' && +choice < 1 || +choice > 9) {
            console.log('-------------------------')
            console.log('Enter your choice again:');
            console.log('-------------------------')
        }
        currentPlayer++;

        let isTurn = makeTurn(field, currentPlayer % 2 ? player0 : playerX, (+choice) - 1);

        status = gameStatus(field);
        if (!isTurn) {
            console.log('-------------------------')
            console.log('Enter your choice again:');
            console.log('-------------------------')
            currentPlayer--;
            continue;
        }
        if (status === 'x') {
            console.log(renderField(field));
            console.log('Congratulations [x] win!');
        } else if (status === '0') {
            console.log(renderField(field));
            console.log('Congratulations [0] win!');
        } else if (status === 'end') {
            console.log(renderField(field));
            console.log('It\'s a tie!');
        }
    }
}
game();