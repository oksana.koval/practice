const root = document.getElementById('root');
const tweetItems = document.getElementById('tweetItems');
const modifyItem = document.getElementById('modifyItem');
const modifyItemHeader = document.getElementById('modifyItemHeader');
const modifyItemInput = document.getElementById('modifyItemInput');
const addTweet = document.querySelector('.addTweet');
const cancelModification = document.getElementById('cancelModification');
const saveModifiedItem = document.getElementById('saveModifiedItem');
const list = document.getElementById('list');
const alertMessage = document.getElementById('alertMessage');
const alertMessageText = document.getElementById('alertMessageText');
const navigationButtons = document.getElementById('navigationButtons');
const pageName = document.getElementsByTagName('h1');

alertMessage.className = 'alert-hidden';

modifyItem.hidden = true;
let tweetsArray = localStorage.getItem('tweets')
    ? JSON.parse(localStorage.getItem('tweets'))
    : [];

window.onpopstate = function(event) {
    if (event.state === null) {
        showTweetItems();
    } else if (event.state.page === `edit`) {
        modifyItemInput.value = tweetsArray[+event.state.id].text;
        tweetItems.hidden = true;
        modifyItem.hidden = false;
    } else if (event.state.page === 'liked') {
        pageName[0].innerText = 'Liked Tweets';
        showTweets(true);
        likedButton.hidden = true;
        addTweet.hidden = true;
        backButton.hidden = false;
    } else if (event.state.page === 'add') {
        tweetItems.hidden = true;
        modifyItem.hidden = false;
        modifyItemHeader.innerText = 'Add tweet';
    }
}

class Tweet {
    constructor(id, text) {
        this.id = id;
        this.text = text;
        this.like = false;
    }
}

function showModifyItem() {
    tweetItems.hidden = true;
    modifyItem.hidden = false;
    modifyItemHeader.innerText = 'Add tweet';
    history.pushState({page: 'add'}, 'Add Tweet', '#/add');
}

function showTweetItems() {
    modifyItem.hidden = true;
    tweetItems.hidden = false;
    pageName[0].innerText = 'Simple Twitter';
    addTweet.hidden = false;
    backButton.hidden = true;
    showTweets(false);
}

function addTweets() {
    let isDuplicates = tweetsArray.filter(el => el.text === modifyItemInput.value).length > 0;
    let id = tweetsArray.length > 0 ? tweetsArray[tweetsArray.length-1].id + 1 : 0;
    let tweet = new Tweet(id, modifyItemInput.value);
    if (modifyItemInput.value !== '' && !isDuplicates) {
        tweetsArray.push(tweet);
        localStorage.setItem('tweets', JSON.stringify(tweetsArray));
        modifyItemInput.value = '';
        showTweetItems();
        showTweets(false);
        list.hidden = false;
    } else if (modifyItemInput.value === '') {
        return;
    } else if (modifyItemInput.value !== '' && isDuplicates) {
        alertMessageText.innerText = 'Error! You can\'t tweet about that';
        alertMessage.className = 'alert';
        setTimeout('alertMessage.className = "alert-hidden"', 2000);
    }

}

function showTweets(isFiltered) {
    let tweets;
    likedButton.hidden = !tweetsArray.some(el => el.like);
    if (isFiltered) {
        tweets = tweetsArray.filter(el => el.like);
    } else {
        tweets = tweetsArray;
    }
    list.innerHTML = '';
    tweets.forEach((tweet) => {
        let span = document.createElement('span');
        span.setAttribute('id', 'spanForButtons')
        let spanForText = document.createElement('span');
        spanForText.setAttribute('class', 'spanForText');
        spanForText.id = tweet.id;
        let removeButton = document.createElement('button');
        removeButton.setAttribute('class', 'removeButton');
        removeButton.id = tweet.id;
        removeButton.innerText = 'remove';
        removeButton.addEventListener('click', removeTweet);
        span.appendChild(removeButton);
        let likeButton = document.createElement('button');
        likeButton.setAttribute('class', 'likeButton');
        likeButton.id = tweet.id;
        likeButton.innerText = tweet.like ? 'unlike' : 'like';
        likeButton.addEventListener('click', likeTweet);
        span.appendChild(likeButton);
        let li = document.createElement('li');
        spanForText.innerText = tweet.text;
        li.appendChild(spanForText);
        li.appendChild(span);
        spanForText.addEventListener('click', editTweet);
        list.appendChild(li);
    });
}

function removeTweet(e) {
    let index = tweetsArray.findIndex(el => el.id === +e.target.id);
    tweetsArray.splice(index, 1);
    localStorage.setItem('tweets', JSON.stringify(tweetsArray));
    showTweets(false);
}

let backButton = document.createElement('button');
backButton.setAttribute('id', 'backButton');
backButton.innerText = 'back';
navigationButtons.appendChild(backButton);
backButton.hidden = true;
backButton.addEventListener('click', showMainPage)

function goToLiked() {
    pageName[0].innerText = 'Liked Tweets';
    showTweets(true);
    likedButton.hidden = true;
    addTweet.hidden = true;
    backButton.hidden = false;
    history.pushState({page: 'liked'}, 'Like', '#/liked');
}

function showMainPage() {
    pageName[0].innerText = 'Simple Twitter';
    showTweets(false);
    addTweet.hidden = false;
    backButton.hidden = true;
}

let likedButton = document.createElement('button');
likedButton.id = 'likedButton';
likedButton.innerText = 'Go to liked';
navigationButtons.appendChild(likedButton);
likedButton.hidden = true;
likedButton.addEventListener('click', goToLiked);

function likeTweet(e) {
    let index = tweetsArray.findIndex(el => el.id === +e.target.id);
    if (tweetsArray[index].like === false) {
        tweetsArray[index].like = true;
        alertMessageText.innerText = `Hooray! You liked tweet with id ${index}!`;
        alertMessage.className = 'alert';
        setTimeout('alertMessage.className = "alert-hidden"', 2000);
    } else {
        tweetsArray[index].like = false;
    }

    localStorage.setItem('tweets', JSON.stringify(tweetsArray));
    showTweets(false);
}

function editTweet(e) {
    let index = tweetsArray.findIndex(el => el.id === +e.target.id);
    modifyItemInput.value = tweetsArray[index].text;
    tweetItems.hidden = true;
    modifyItem.hidden = false;
    history.pushState({page: 'edit', id: index}, 'Edit', `#/edit/${index}`);
}

cancelModification.addEventListener('click', showTweetItems);
addTweet.addEventListener('click', showModifyItem);
saveModifiedItem.addEventListener('click', addTweets);
showTweets();