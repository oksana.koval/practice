function visitLink(path) {
    let links = localStorage.getItem('path');
    if (links === null) {
        links = {};
        links[path] = 1;
        localStorage.setItem('path', JSON.stringify(links));
    } else {
        links = JSON.parse(links);
        if (links[path] === undefined) {
            links[path] = 1;
        } else {
            links[path] += 1;
        }

        localStorage.setItem('path', JSON.stringify(links));
    }
}

function viewResults() {
    if (localStorage.getItem('path') !== null) {
        let links = JSON.parse(localStorage.getItem('path'));
        let ul = document.createElement('ul');
        for (let i in links) {
            if (i) {
                let li = document.createElement('li');
                li.innerHTML = `You visited ${i} ${links[i]} time(s)`;
                ul.appendChild(li);
            }
        }

        document.getElementById('content').appendChild(ul);
        localStorage.clear();
    }
}
