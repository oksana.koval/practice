function insertCharacter(char) {
    let currentValue = $('.display').val();
    let length = currentValue.length;
    let flag = false;
    if (char === '+' || char === '-' || char === '*' || char === '/') {
        flag = true;
    }
    if (length === 0 && flag) {
        return;
    }
    let flagNew = false;
    let lastCharacter = currentValue[length - 1];
    if (lastCharacter === '+' || lastCharacter === '-' || lastCharacter === '*' || lastCharacter === '/') {
        flagNew = true;
    }
    if (flag && flagNew) {
        $('.display').val(currentValue.substring(0, length - 1) + char);
    } else {
        $('.display').val($('.display').val() + char);
    }
}

function clearInput() {
    $('.display').val('').css('color', 'black');
}

function calculateResult(str) {
    let operatorToFunction = {
        '+': (num1, num2) => +num1 + +num2,
        '-': (num1, num2) => +num1 - +num2,
        '*': (num1, num2) => +num1 * +num2,
        '/': (num1, num2) => +num1 / +num2
    }

    function findOperator(str) {
        const [operator] = str.split('').filter((ch) => ['+', '-', '*', '/'].includes(ch));
        return operator;
    }

    let operationStr = str.replace(/[ ]/g, '');
    let operator = findOperator(operationStr);
    let [num1, num2] = operationStr.split(operator);
    return Math.round(operatorToFunction[operator](num1, num2) * 10000) / 10000;
}

function result() {
    let currentValue = $('.display').val();
    let length = currentValue.length;
    let flag = false;
    let char = currentValue[length - 1];
    if (char === '+' || char === '-' || char === '*' || char === '/') {
        flag = true;
    }
    if (flag || calculateResult($('.display').val()) === Infinity) {
        $('.display').val('ERROR').css('color', 'red');
    } else {
        $('.display').val(calculateResult($('.display').val()));
    }
    if ($('.display').val() !== 'ERROR') {
        createLogs(currentValue, $('.display').val());
    }
}

function createLogs(expr, result) {
    let $div = $('<div>', {'class': 'log-container'});
    $('.log').prepend($div);
    let $closeSign = $('<div>', {'class': 'close-sign'});
    $closeSign.html('&#x2715');
    $closeSign.click(function () {
        $(this).parent().remove();
    });
    $div.prepend($closeSign);
    let equation = $('<div>', {'class': 'equation'});
    equation.html(`${expr}=${result}`);
    $div.prepend(equation);
    let $circle = $('<div>', {'class': 'circle'});
    $circle.click(function () {
        $(this).toggleClass('red-background');
    });
    $div.prepend($circle);

    $('.equation:contains(48)').css('text-decoration', 'underline');
}

$('.log').scroll(function () {
    console.log('Scroll Top: ' + $('.log').scrollTop());
});
