const url = 'https://jsonplaceholder.typicode.com/users';
let updateButton;
let deleteButton;
let userCard;
let data;
let loader;

const getData = async () => {
    try {
        const response = await fetch(url);
        if (response.ok) {
            const jsonResponse = await response.json();
            showResponse(jsonResponse);
            data = jsonResponse;
        }
    } catch (error) {
        console.log(error);
    }
}

getData();

function showResponse(data) {
    data.forEach((user, index.js) => {
        const users = document.getElementById('users');

        userCard = document.createElement('div');
        userCard.setAttribute('class', 'user-card');
        userCard.setAttribute('id', `card-${index.js}`);
        users.appendChild(userCard);

        const userName = document.createElement('h1');
        userName.setAttribute('class', 'user-name');
        userName.setAttribute('contenteditable', 'true');
        userCard.appendChild(userName);

        const p0 = document.createElement('p');
        p0.innerHTML = 'Username: '
        userCard.appendChild(p0);
        const span0 = document.createElement('span');
        span0.setAttribute('class', 'username');
        span0.setAttribute('contenteditable', 'true');
        p0.appendChild(span0);
        const p1 = document.createElement('p');
        p1.innerHTML = 'ID: '
        userCard.appendChild(p1);
        const span1 = document.createElement('span');
        span1.setAttribute('class', 'user-id');
        span1.setAttribute('contenteditable', 'true');
        p1.appendChild(span1);
        const p2 = document.createElement('p');
        p2.innerHTML = 'Phone: '
        userCard.appendChild(p2);
        const span2 = document.createElement('span');
        span2.setAttribute('class', 'user-phone');
        span2.setAttribute('contenteditable', 'true');
        p2.appendChild(span2);
        const p3 = document.createElement('p');
        p3.innerHTML = 'Website: '
        userCard.appendChild(p3);
        const span3 = document.createElement('span');
        span3.setAttribute('class', 'user-website');
        span3.setAttribute('contenteditable', 'true');
        p3.appendChild(span3);
        const p13 = document.createElement('p');
        p13.innerHTML = 'Email: '
        userCard.appendChild(p13);
        const span13 = document.createElement('span');
        span13.setAttribute('class', 'user-email');
        span13.setAttribute('contenteditable', 'true');
        p13.appendChild(span13);

        const address = document.createElement('h2');
        address.innerHTML = 'Address';
        userCard.appendChild(address);

        const p4 = document.createElement('p');
        p4.innerHTML = 'City: '
        userCard.appendChild(p4);
        const span4 = document.createElement('span');
        span4.setAttribute('class', 'city');
        span4.setAttribute('contenteditable', 'true');
        p4.appendChild(span4);
        const p7 = document.createElement('p');
        p7.innerHTML = 'Street: '
        userCard.appendChild(p7);
        const span7 = document.createElement('span');
        span7.setAttribute('class', 'street');
        span7.setAttribute('contenteditable', 'true');
        p7.appendChild(span7);
        const p8 = document.createElement('p');
        p8.innerHTML = 'Suite: '
        userCard.appendChild(p8);
        const span8 = document.createElement('span');
        span8.setAttribute('class', 'suite');
        span8.setAttribute('contenteditable', 'true');
        p8.appendChild(span8);
        const p9 = document.createElement('p');
        p9.innerHTML = 'Zipcode: '
        userCard.appendChild(p9);
        const span9 = document.createElement('span');
        span9.setAttribute('class', 'zipcode');
        span9.setAttribute('contenteditable', 'true');
        p9.appendChild(span9);

        const geo = document.createElement('h3');
        geo.innerHTML = 'Geo';
        userCard.appendChild(geo);
        const div1 = document.createElement('div');
        div1.setAttribute('class', 'geo-container');
        userCard.appendChild(div1);
        const div2 = document.createElement('div');
        div1.appendChild(div2);
        const div3 = document.createElement('div');
        div1.appendChild(div3);
        const p5 = document.createElement('p');
        p5.innerHTML = 'Lat: '
        div2.appendChild(p5);
        const span5 = document.createElement('span');
        span5.setAttribute('class', 'lat');
        span5.setAttribute('contenteditable', 'true');
        p5.appendChild(span5);
        const p6 = document.createElement('p');
        p6.innerHTML = 'Lng: '
        div3.appendChild(p6);
        const span6 = document.createElement('span');
        span6.setAttribute('class', 'lng');
        span6.setAttribute('contenteditable', 'true');
        p6.appendChild(span6);

        const company = document.createElement('h2');
        company.innerHTML = 'Company';
        userCard.appendChild(company);

        const p10 = document.createElement('p');
        p10.innerHTML = 'Company Name: '
        userCard.appendChild(p10);
        const span10 = document.createElement('span');
        span10.setAttribute('class', 'company-name');
        span10.setAttribute('contenteditable', 'true');
        p10.appendChild(span10);
        const p11 = document.createElement('p');
        p11.innerHTML = 'Catch Phrase: '
        userCard.appendChild(p11);
        const span11 = document.createElement('span');
        span11.setAttribute('class', 'catch-phrase');
        span11.setAttribute('contenteditable', 'true');
        p11.appendChild(span11);
        const p12 = document.createElement('p');
        p12.innerHTML = 'Bs: '
        userCard.appendChild(p12);
        const span12 = document.createElement('span');
        span12.setAttribute('class', 'bs');
        span12.setAttribute('contenteditable', 'true');
        p12.appendChild(span12);

        const div = document.createElement('div');
        div.setAttribute('class', 'button-container');
        userCard.appendChild(div);
        updateButton = document.createElement('button');
        updateButton.setAttribute('id', `update-${index.js}`);
        updateButton.innerText = 'UPDATE';
        div.appendChild(updateButton);
        deleteButton = document.createElement('button');
        deleteButton.setAttribute('id', `delete-${index.js}`);
        deleteButton.innerText = 'DELETE';
        div.appendChild(deleteButton);

        const div4 = document.createElement('div');
        div4.setAttribute('class', 'loader-container');
        div4.setAttribute('id', `loader-${index.js}`);
        userCard.appendChild(div4);
        loader = document.createElement('div');
        loader.setAttribute('class', 'loader');
        div4.appendChild(loader);

        userName.innerHTML = user.name;
        span0.innerHTML = user.username;
        span1.innerHTML = user.id;
        span2.innerHTML = user.phone;
        span3.innerHTML = user.website;
        span13.innerHTML = user.email;
        span4.innerHTML = user.address.city;
        span5.innerHTML = user.address.geo.lat;
        span6.innerHTML = user.address.geo.lng;
        span7.innerHTML = user.address.street;
        span8.innerHTML = user.address.suite;
        span9.innerHTML = user.address.zipcode;
        span10.innerHTML = user.company.name;
        span11.innerHTML = user.company.catchPhrase;
        span12.innerHTML = user.company.bs;

        updateButton.addEventListener('click', updateCard);
        deleteButton.addEventListener('click', deleteCard);
    });
}

function hideLoader(id) {
    document.getElementById(`loader-${id}`).style.visibility = 'hidden';
}


function showLoader(id) {
    document.getElementById(`loader-${id}`).style.visibility = 'visible';
}

function updateCard(e) {
    let updateId = +e.target.id.slice(-1);
    let cardId = document.getElementById(`card-${updateId}`);

    let userName = cardId.getElementsByClassName('user-name')[0].innerText;
    data[updateId].name = userName;

    let username = cardId.getElementsByClassName('username')[0].innerText;
    data[updateId].username = username;

    let userId = cardId.getElementsByClassName('user-id')[0].innerText;
    data[updateId].id = +userId;

    let userPhone = cardId.getElementsByClassName('user-phone')[0].innerText;
    data[updateId].phone = userPhone;

    let userWebsite = cardId.getElementsByClassName('user-website')[0].innerText;
    data[updateId].website = userWebsite;

    let userEmail = cardId.getElementsByClassName('user-email')[0].innerText;
    data[updateId].email = userEmail;

    let city = cardId.getElementsByClassName('city')[0].innerText;
    data[updateId].address.city = city;

    let street = cardId.getElementsByClassName('street')[0].innerText;
    data[updateId].address.street = street;

    let suite = cardId.getElementsByClassName('suite')[0].innerText;
    data[updateId].address.suite = suite;

    let zipcode = cardId.getElementsByClassName('zipcode')[0].innerText;
    data[updateId].address.zipcode = zipcode;

    let lat = cardId.getElementsByClassName('lat')[0].innerText;
    data[updateId].address.geo.lat = lat;

    let lng = cardId.getElementsByClassName('lng')[0].innerText;
    data[updateId].address.geo.lng = lng;

    let companyName = cardId.getElementsByClassName('company-name')[0].innerText;
    data[updateId].company.name = companyName;

    let catchPhrase = cardId.getElementsByClassName('catch-phrase')[0].innerText;
    data[updateId].company.catchPhrase = catchPhrase;

    let bs = cardId.getElementsByClassName('bs')[0].innerText;
    data[updateId].company.bs = bs;

    let updateData = JSON.stringify(data[updateId]);
    putData(updateData, updateId);
}

const putData = async (data, id) => {
    showLoader(id);
    try {
        const response = await fetch(`${url}/${id + 1}`, {
            method: 'PUT',
            body: data,
            headers: {
                'Content-type': 'application/json'
            }
        })
        if (response.ok) {
            hideLoader(id);
        }
    } catch (error) {
        console.log(error);
    }
}

function deleteCard(e) {
    let deleteId = +e.target.id.slice(-1);
    let cardId = document.getElementById(`card-${deleteId}`);
    data = data.filter(el => el.id !== deleteId + 1);
    cardId.remove();

    let deleteCard = JSON.stringify(data[deleteId]);
    deleteData(deleteCard, deleteId);
}

const deleteData = async (data, id) => {
    showLoader(id + 1);
    try {
        const response = await fetch(`${url}/${id + 1}`, {
            method: 'DELETE'
        })
        if (response.ok) {
            hideLoader(id + 1);
        }
    } catch (error) {
        console.log(error);
    }
}
