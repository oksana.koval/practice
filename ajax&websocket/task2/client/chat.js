class Message {
    constructor(name, message, time) {
        this.name = name;
        this.message = message;
        this.time = time;
    }
}

let username;
let message;

const chatContainer = document.getElementById('chat-container');
const form = document.getElementById('form');
const userMessage = document.getElementById('user-message');

let socket = new WebSocket('ws://localhost:8080');

socket.onopen = function () {
    username = prompt('Please enter your name:');
};

socket.onclose = function () {
    console.log('Connection closed.');
}

socket.onmessage = function (response) {
    let obj = JSON.parse(response.data);
    showMessage(obj, false);
}

form.addEventListener('submit', e => {
    e.preventDefault();
    let msg = new Message(username, userMessage.value, getTime())
    socket.send(JSON.stringify(msg));
    showMessage(msg, true);
    userMessage.value = '';
})

function showMessage(object, isSender) {
    const messageContainer = document.createElement('div');
    messageContainer.setAttribute('class', `message-container ${isSender ? 'sent' : 'received'}`);
    chatContainer.appendChild(messageContainer);

    const userName = document.createElement('p');
    userName.setAttribute('class', 'user-name');
    userName.innerText = object.name;
    messageContainer.appendChild(userName);

    const messageContent = document.createElement('p');
    messageContent.setAttribute('class', 'message-content');
    messageContent.innerText = object.message;

    const time = document.createElement('span');
    time.setAttribute('class', 'time');
    time.innerText = object.time;
    messageContent.appendChild(time);
    messageContainer.appendChild(messageContent);
}

function getTime() {
    return new Date().toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
}