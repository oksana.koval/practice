function getMaxEvenElement(arr) {
    return arr.reduce((a, b) => {
        if (b % 2 === 0 && Math.max(a, b) > a) {
            return b;
        }
        return a;
    }, 0);
}

let a = 3;
let b = 5;

[a, b] = [b, a];

function getValue(value) {
    return value ?? '-';
}

function getObjFromArray(arr) {
    return Object.fromEntries(arr);
}

function addUniqueId(obj) {
    let newObj = Object.assign({}, obj);
    newObj['id'] = Symbol();
    return newObj;
}

function getRegroupedObject(obj) {
    let {name, details: {id, age, university}} = obj;
    return {university: university, user: {age, firstName: name, id}};
}

function getArrayWithUniqueElements(arr) {
    return [...new Set(arr)];
}

function hideNumber(number) {
    return number.slice(-4).padStart(number.length, '*');
}

function isRequired(arg) {
    throw new Error(`${arg} is required`);
}

function add(a = isRequired('a'), b = isRequired('b')) {
    return a + b;
}

function* generateIterableSequence() {
    yield 'I';
    yield 'love';
    yield 'JS';
}