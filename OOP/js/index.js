'use strict';

const price = {
    'SMALL': 50,
    'MEDIUM': 75,
    'LARGE': 100,
    'VEGGIE': 50,
    'MARGHERITA': 60,
    'PEPPERONI': 70,
    'CHEESE': 7,
    'TOMATOES': 5,
    'MEAT': 9
};

const ingredients = [];

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */
class Pizza {
    constructor(size, type) {
        this.size = size;
        this.type = type;

        if (this.size === undefined || this.type === undefined) {
            throw new PizzaException(`Required two arguments, given: ${arguments.length}`);
        } else if (!Pizza.allowedSizes.includes(this.size)) {
            throw new PizzaException('Invalid size');
        } else if (!Pizza.allowedTypes.includes(this.type)) {
            throw new PizzaException('Invalid type');
        }
    }

    getSize() {
        return this.size;
    }

    getPrice() {
        let sum = 0;
        for (let key in price) {
            if (key) {
                if (this.size === key || this.type === key) {
                    sum += price[key];
                }
                for (let e of ingredients) {
                    if (e === key) {
                        sum += price[key];
                    }
                }
            }
        }
        return sum;
    }

    addExtraIngredient(ingredient) {
        if (ingredients.includes(ingredient)) {
            throw new PizzaException('Duplicate ingredient');
        } else if (!Pizza.allowedExtraIngredients.includes(ingredient)) {
            throw new PizzaException('Invalid ingredient');
        } else if (arguments.length > 1) {
            throw new PizzaException('Accepted only one ingredient');
        } else {
            ingredients.push(ingredient);
        }
    }

    removeExtraIngredient(ingredient) {
        let index.js = ingredients.indexOf(ingredient);

        if (!ingredients.includes(ingredient)) {
            throw new PizzaException('Ingredient has not been added');
        } else if (!Pizza.allowedExtraIngredients.includes(ingredient)) {
            throw new PizzaException('Invalid ingredient');
        } else if (arguments.length > 1) {
            throw new PizzaException('Accepted only one ingredient');
        } else if (index.js > -1) {
            ingredients.splice(index.js, 1);
        }
    }

    getExtraIngredients() {
        return ingredients;
    }

    getPizzaInfo() {
        return 'Size: ' + this.getSize() +', type: ' + this.type + '; extra ingredients: ' +
            ingredients.join(',') + '; price: ' + this.getPrice() + ' UAH.';
    }
}

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = 'SMALL';
Pizza.SIZE_M = 'MEDIUM';
Pizza.SIZE_L = 'LARGE';

Pizza.TYPE_VEGGIE = 'VEGGIE';
Pizza.TYPE_MARGHERITA = 'MARGHERITA';
Pizza.TYPE_PEPPERONI = 'PEPPERONI';

Pizza.EXTRA_TOMATOES = 'TOMATOES';
Pizza.EXTRA_CHEESE = 'CHEESE';
Pizza.EXTRA_MEAT = 'MEAT';

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI];
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT];

/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */

class PizzaException {
    constructor(log) {
        this.log = log;
    }
}

/* It should work */
// // small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_CORN); // => Invalid ingredient