class BirthdayService {
    async howLongToMyBirthday(inputDate) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (inputDate instanceof Date) {
                    let dateNow = new Date();
                    let thisYearBirthday = inputDate.setUTCFullYear(dateNow.getFullYear());
                    let diffTime = `${(dateNow - thisYearBirthday) / (1000 * 60 * 60 * 24)}`;
                    let diffDays = parseInt(diffTime, 10);
                    resolve(diffDays === 0 ? this.congratulateWithBirthday() : this.notifyWaitingTime(diffDays));
                } else {
                    reject(new Error('Wrong argument!'));
                }
            }, 100);
        });
    }

    congratulateWithBirthday() {
        return logger('Hooray!!! It is today!');
    }

    notifyWaitingTime(waitingTime) {
        return waitingTime < 183
            ? logger(`Oh, you have celebrated it ${waitingTime} day/s ago, don't you remember?`)
            : logger(`Soon...Please, wait just ${waitingTime} day/days`);
    }
}

function logger(value) {
    console.log(value);
    return value;
}

module.exports = BirthdayService;