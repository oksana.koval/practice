const getBigestNumber = (...args) => {
    if (args.length < 2) {
        throw new Error('Not enough arguments');
    } else if (args.length > 10) {
        throw new Error('Too many arguments');
    }
    return Math.max(...args);
}

module.exports = getBigestNumber;
