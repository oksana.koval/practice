const BirthdayService = require('../src/birthday.service');
describe('BirthdayService', () => {
    const birthdayService = new BirthdayService();
    const oneDayInMilliseconds = 24 * 60 * 60 * 1000;
    it('howLongToMyBirthday() should return "Hooray!!! It is today!" when input date is today',
        async () => {
        const result = await birthdayService.howLongToMyBirthday(new Date());
        expect(result).toBe('Hooray!!! It is today!');
    });

    it('howLongToMyBirthday() should return an Error "Wrong argument!", when argument is not a Date',
        async () => {
        await expectAsync(birthdayService
            .howLongToMyBirthday('test'))
            .toBeRejectedWithError('Wrong argument!');
    });

    it('howLongToMyBirthday() should return "Oh, you have celebrated it ..." when waitingTime < 183',
        async () => {
        const result = await birthdayService
            .howLongToMyBirthday(new Date(new Date().getTime() - 2 * oneDayInMilliseconds));
        expect(result).toContain('Oh, you have celebrated it');
    });

    it('howLongToMyBirthday() should return "Soon...Please, wait just ..." when waitingTime > 183',
        async () => {
        const result = await birthdayService
            .howLongToMyBirthday(new Date(new Date().getTime() - 200 * oneDayInMilliseconds));
        expect(result).toContain('Soon...Please, wait just');
    });

});