const getBigestNumber = require('../src/get-bigest-number');

describe('Get biggest number test', () => {
    it('getBigestNumber() should return the biggest number in normal condition',
        async () => {
        const result = getBigestNumber(1, 2, 3);
        expect(result).toBe(3);
    });

    it('getBigestNumber() should return an Error "Too many arguments", when arguments count more then 10',
        async () => {
        expect(function() {
            getBigestNumber(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
        }).toThrow(new Error('Too many arguments'));
    });

    it('getBigestNumber() should return an Error "Not enough arguments", when arguments count less then 2',
        async () => {
        expect(function() {
            getBigestNumber(1)
        }).toThrow(new Error('Not enough arguments'));
    });
});