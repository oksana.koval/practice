const table = document.getElementById('table');
let td = [...document.getElementsByTagName('td')];
let tr = [...document.getElementsByTagName('tr')];

function changeColor(e) {
    td.forEach(() => {
        if (e.target.id === 'special-cell') {
            table.style.backgroundColor = '#347f1c';
        } else if (e.target.className === 'first-col') {
            tr.forEach(() => {
                switch (e.path[1].id) {
                    case 'first':
                        tr[0].style.backgroundColor = '#0000f5';
                        break;
                    case 'second':
                        tr[1].style.backgroundColor = '#0000f5';
                        break;
                    case 'third':
                        tr[2].style.backgroundColor = '#0000f5';
                        break;
                    default:
                        break;
                }
            })
        } else {
            e.target.style.backgroundColor = '#ffff53';
        }
    })
}

td.forEach(td => td.addEventListener('click', changeColor));

let resultDiv = document.getElementById('result');
let button = document.getElementById('send');
let phone = document.getElementById('phone');

function isValidNumber(e) {
    let pattern = /^\+380\d{9}$/;
    if (pattern.test(e.target.value)) {
        button.disabled = false;
        resultDiv.setAttribute('class', 'hidden')
        phone.removeAttribute('class');
    } else {
        phone.setAttribute('class', 'invalid-input');
        button.disabled = true;
        resultDiv.setAttribute('class', 'failure');
        resultDiv.innerText = 'Type number does not follow format +380*********';
    }
}

function onSend(e) {
    resultDiv.setAttribute('class', 'success');
    resultDiv.innerText = 'Data was successfully sent';
    e.preventDefault();
}

phone.addEventListener('keyup', isValidNumber);
button.addEventListener('click', onSend);

const ballPosX = 280;
const ballPosY = 145;
const teamAX = 35;
const teamAY = 165;
const teamBX = 565;
const teamBY = 165;
const scoreA = document.getElementById('teamA');
const scoreB = document.getElementById('teamB');
const game = document.getElementById('game');
const fieldCanvas = document.getElementById('canvas');
const canvasContext = fieldCanvas.getContext('2d');
const teamAGoalEvent = new CustomEvent('goal', {
    detail: {
        text: 'Team A score!',
        className: 'teamA'
    }
});

const teamBGoalEvent = new CustomEvent('goal', {
    detail: {
        text: 'Team B score!',
        className: 'teamB'
    }
});

let scoreTeamA = 0;
let scoreTeamB = 0;

fieldCanvas.width = 600;
fieldCanvas.height = 330;

let backgroundImage = new Image();
backgroundImage.onload = function () {
    canvasContext.drawImage(backgroundImage, 0, 0);
}

let ballImage = new Image();
ballImage.src = 'assets/ball.png';

ballImage.onload = function () {
    canvasContext.drawImage(ballImage, ballPosX, ballPosY, 40, 40);
}

function getPosition(canvas, evt) {
    let rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

function move(e) {
    let rect = fieldCanvas.getBoundingClientRect();
    canvasContext.clearRect(0, 0, fieldCanvas.width, fieldCanvas.height);
    let mousePos = getPosition(fieldCanvas, e);
    canvasContext.drawImage(ballImage, mousePos.x - 15, mousePos.y - 10, 40, 40);

    let ballX = e.clientX - rect.left;
    let ballY = e.clientY - rect.top;

    let halfOfDiagonal = 7.5;

    if (ballX > teamAX - halfOfDiagonal && ballX < teamAX + halfOfDiagonal
        && (ballY > teamAY - halfOfDiagonal && ballY < teamAY + halfOfDiagonal)) {
        scoreTeamA += 1;
        fieldCanvas.dispatchEvent(teamAGoalEvent);
    } else if (ballX > teamBX - halfOfDiagonal && ballX < teamBX + halfOfDiagonal
        && (ballY > teamBY - halfOfDiagonal && ballY < teamBY + halfOfDiagonal)) {
        scoreTeamB += 1;
        fieldCanvas.dispatchEvent(teamBGoalEvent);
    }

    scoreA.innerText = `Team A:${scoreTeamA}`;
    scoreB.innerText = `Team B:${scoreTeamB}`;
}

function showMessage(e) {
    let messageDiv = document.createElement('div');
    messageDiv.setAttribute('class', e.detail.className);
    messageDiv.innerText = e.detail.text;
    game.appendChild(messageDiv);
    setTimeout(() => game.removeChild(messageDiv), 3000);
}

fieldCanvas.addEventListener('goal', showMessage);
fieldCanvas.addEventListener('click', move);
