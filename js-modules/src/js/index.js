import {tweetItems, modifyItem, modifyItemHeader, modifyItemInput, addTweet } from './variables';
import {cancelModification, saveModifiedItem, alertMessage} from './variables';
import {alertMessageText, navigationButtons, pageName} from './variables';
import {showTweets} from './showTweets';
import {addTweets} from './addTweets';
import '../scss/styles.scss';

alertMessage.className = 'alert-hidden';

modifyItem.hidden = true;
export let tweetsArray = localStorage.getItem('tweets')
    ? JSON.parse(localStorage.getItem('tweets'))
    : [];

window.onpopstate = function(event) {
    if (event.state === null) {
        showTweetItems();
    } else if (event.state.page === `edit`) {
        modifyItemInput.value = tweetsArray[+event.state.id].text;
        tweetItems.hidden = true;
        modifyItem.hidden = false;
    } else if (event.state.page === 'liked') {
        pageName[0].innerText = 'Liked Tweets';
        showTweets(true);
        likedButton.hidden = true;
        addTweet.hidden = true;
        backButton.hidden = false;
    } else if (event.state.page === 'add') {
        tweetItems.hidden = true;
        modifyItem.hidden = false;
        modifyItemHeader.innerText = 'Add tweet';
    }
}

export class Tweet {
    constructor(id, text) {
        this.id = id;
        this.text = text;
        this.like = false;
    }
}

const showModifyItem = () => {
    tweetItems.hidden = true;
    modifyItem.hidden = false;
    modifyItemHeader.innerText = 'Add tweet';
    history.pushState({page: 'add'}, 'Add Tweet', '#/add');
}

export const showTweetItems = () => {
    modifyItem.hidden = true;
    tweetItems.hidden = false;
    pageName[0].innerText = 'Simple Twitter';
    addTweet.hidden = false;
    backButton.hidden = true;
    showTweets(false);
}

export const removeTweet = (e) => {
    let index = tweetsArray.findIndex(el => el.id === +e.target.id);
    tweetsArray.splice(index, 1);
    localStorage.setItem('tweets', JSON.stringify(tweetsArray));
    showTweets(false);
}

const showMainPage = () => {
    pageName[0].innerText = 'Simple Twitter';
    showTweets(false);
    addTweet.hidden = false;
    backButton.hidden = true;
}

let backButton = document.createElement('button');
backButton.setAttribute('id', 'backButton');
backButton.innerText = 'back';
navigationButtons.appendChild(backButton);
backButton.hidden = true;
backButton.addEventListener('click', showMainPage)

const goToLiked = () => {
    pageName[0].innerText = 'Liked Tweets';
    showTweets(true);
    likedButton.hidden = true;
    addTweet.hidden = true;
    backButton.hidden = false;
    history.pushState({page: 'liked'}, 'Like', '#/liked');
}

export let likedButton = document.createElement('button');
likedButton.id = 'likedButton';
likedButton.innerText = 'Go to liked';
navigationButtons.appendChild(likedButton);
likedButton.hidden = true;
likedButton.addEventListener('click', goToLiked);

export const likeTweet = (e) => {
    let index = tweetsArray.findIndex(el => el.id === +e.target.id);
    if (tweetsArray[index].like === false) {
        tweetsArray[index].like = true;
        alertMessageText.innerText = `Hooray! You liked tweet with id ${index}!`;
        alertMessage.className = 'alert';
        setTimeout('alertMessage.className = "alert-hidden"', 2000);
    } else {
        tweetsArray[index].like = false;
    }

    localStorage.setItem('tweets', JSON.stringify(tweetsArray));
    showTweets(false);
}

export const editTweet = (e) => {
    let index = tweetsArray.findIndex(el => el.id === +e.target.id);
    modifyItemInput.value = tweetsArray[index].text;
    tweetItems.hidden = true;
    modifyItem.hidden = false;
    history.pushState({page: 'edit', id: index}, 'Edit', `#/edit/${index}`);
}

cancelModification.addEventListener('click', showTweetItems);
addTweet.addEventListener('click', showModifyItem);
saveModifiedItem.addEventListener('click', addTweets);
showTweets();