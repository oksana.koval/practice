import {tweetsArray} from './index';
import {likedButton} from './index';
import {removeTweet} from './index';
import {likeTweet} from './index';
import {editTweet} from './index';
import {list} from './variables';

export const showTweets = (isFiltered) => {
    let tweets;
    likedButton.hidden = !tweetsArray.some(el => el.like);
    if (isFiltered) {
        tweets = tweetsArray.filter(el => el.like);
    } else {
        tweets = tweetsArray;
    }
    list.innerHTML = '';
    tweets.forEach((tweet) => {
        let span = document.createElement('span');
        span.setAttribute('id', 'spanForButtons')
        let spanForText = document.createElement('span');
        spanForText.setAttribute('class', 'spanForText');
        spanForText.id = tweet.id;
        let removeButton = document.createElement('button');
        removeButton.setAttribute('class', 'removeButton');
        removeButton.id = tweet.id;
        removeButton.innerText = 'remove';
        removeButton.addEventListener('click', removeTweet);
        span.appendChild(removeButton);
        let likeButton = document.createElement('button');
        likeButton.setAttribute('class', 'likeButton');
        likeButton.id = tweet.id;
        likeButton.innerText = tweet.like ? 'unlike' : 'like';
        likeButton.addEventListener('click', likeTweet);
        span.appendChild(likeButton);
        let li = document.createElement('li');
        spanForText.innerText = tweet.text;
        li.appendChild(spanForText);
        li.appendChild(span);
        spanForText.addEventListener('click', editTweet);
        list.appendChild(li);
    });
}