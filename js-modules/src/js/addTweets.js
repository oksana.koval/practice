import {tweetsArray} from './index';
import {Tweet} from './index';
import {showTweetItems} from './index';
import {showTweets} from './showTweets';
import {modifyItemInput, alertMessageText, alertMessage, list} from './variables';

export const addTweets = () => {
    let isDuplicates = tweetsArray.filter(el => el.text === modifyItemInput.value).length > 0;
    let id = tweetsArray.length > 0 ? tweetsArray[tweetsArray.length-1].id + 1 : 0;
    let tweet = new Tweet(id, modifyItemInput.value);
    if (modifyItemInput.value !== '' && !isDuplicates) {
        tweetsArray.push(tweet);
        localStorage.setItem('tweets', JSON.stringify(tweetsArray));
        modifyItemInput.value = '';
        showTweetItems();
        showTweets(false);
        list.hidden = false;
    } else if (modifyItemInput.value === '') {
        return;
    } else if (modifyItemInput.value !== '' && isDuplicates) {
        alertMessageText.innerText = 'Error! You can\'t tweet about that';
        alertMessage.className = 'alert';
        setTimeout('alertMessage.className = "alert-hidden"', 2000);
    }

}