function getAge(date) {
    let startOfUnixTime = 1970;
    let today = Date.now();
    let birthDate = new Date(date);
    let difference = today - birthDate;
    let age = new Date(difference);
    return Math.abs(age.getUTCFullYear() - startOfUnixTime);
}

function getWeekDay(inputDate) {
    const options = {weekday: 'long'};
    let date = new Date(inputDate);
    return date.toLocaleDateString('en-US', options);
}

function getAmountDaysToNewYear() {
    let hours = 24;
    let minutes = 60;
    let seconds = 60;
    let milliseconds = 1000;
    let indexOfJanuary = 12;
    let oneDayInMilliseconds = hours * minutes * seconds * milliseconds;
    let today = new Date();
    let newYearDate = new Date(today.getFullYear(), indexOfJanuary, 1);
    return Math.round(Math.abs((today - newYearDate) / oneDayInMilliseconds));
}

function getProgrammersDay(inputYear) {
    let daysInFebruary = 29;
    let indexOfSeptember = 8;
    let programmersDay = 12;
    let leapYear = new Date(inputYear, 1, daysInFebruary).getDate() === daysInFebruary;
    let dayObject = leapYear
        ? new Date(inputYear, indexOfSeptember, programmersDay)
        : new Date(inputYear, indexOfSeptember, programmersDay + 1);
    let day = dayObject.getDate();
    let month = dayObject.toLocaleDateString('en-US', {month: 'short'});
    let year = dayObject.getFullYear();
    let weekday = dayObject.toLocaleDateString('en-US', {weekday: 'long'});
    return `${day} ${month}, ${year} (${weekday})`
}

function howFarIs(inputWeekday) {
    let arrayOfDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let weekdayToLowerCase = inputWeekday.toLowerCase();
    let specifiedWeekday = weekdayToLowerCase.charAt(0).toUpperCase() + weekdayToLowerCase.slice(1);
    let indexOfTodayWeekday = new Date().getDay();
    let indexOfInputWeekday = arrayOfDays.indexOf(specifiedWeekday);
    let daysLeft = Math.abs(indexOfTodayWeekday - indexOfInputWeekday);
    return indexOfTodayWeekday === indexOfInputWeekday
        ? `Hey, today is ${specifiedWeekday} =)`
        : `It's ${daysLeft} day(s) left till ${specifiedWeekday}.`;
}

function isValidIdentifier(inputVariable) {
    let exp = /^[a-zA-Z_$][a-zA-Z_$0-9]*$/;
    return exp.test(inputVariable);
}

function capitalize(str) {
    return str.replace(/(^\w)|(\s+\w)/g, firstLetter => firstLetter.toUpperCase());
}

function isValidAudioFile(str) {
    let exp = /^.[a-zA-Z]*\.(mp3|flac|alac|aac)$/;
    return exp.test(str);
}

function getHexadecimalColors(str) {
    let exp = /[#]\b([a-f0-9]{3}|[a-f0-9]{6})\b/gi;
    let arrayOfHexColors = str.match(exp);
    return arrayOfHexColors !== null ? arrayOfHexColors : [];
}

function isValidPassword(str) {
    let exp = /^(?=.*\d)(?=.*[a-z ])(?=.*[A-Z ])[0-9a-zA-Z ]{8,}$/;
    return exp.test(str);
}

function addThousandsSeparators(inputValue) {
    return inputValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function getAllUrlsFromText(str) {
    let exp = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_.~#?&/=]*)/g;
    let validUrls = str.match(exp);
    if (validUrls !== null) {
        return validUrls;
    } else {
        return [];
    }
}
