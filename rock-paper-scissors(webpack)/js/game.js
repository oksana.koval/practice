import {result, reset, choices} from './variables.js';
import {getWinner} from './getWinner.js';

const scoreboard = {
    player: 0,
    computer: 0,
    round: 0
};

const play = (e) => {
    const playerChoice = e.target.id;
    const computerChoice = getComputerChoice();
    const winner = getWinner(playerChoice, computerChoice);
    showWinner(winner, playerChoice, computerChoice);
}

const getComputerChoice = () => {
    const rand = Math.random();
    if (rand < 0.34) {
        return 'rock';
    } else if (rand <= 0.67) {
        return 'paper';
    } else {
        return 'scissors';
    }
}

const showWinner = (winner, playerChoice, computerChoice) => {
    scoreboard.round++;
    if (winner === 'player') {
        scoreboard.player++;
        result.innerHTML = `<p>Round ${scoreboard.round}, ${playerChoice.charAt(0).toUpperCase() +
        playerChoice.slice(1)} vs. ${computerChoice.charAt(0).toUpperCase() +
        computerChoice.slice(1)}, You've WON!</p>`;
    } else if (winner === 'computer') {
        scoreboard.computer++;
        result.innerHTML = `<p>Round ${scoreboard.round}, ${playerChoice.charAt(0).toUpperCase() +
        playerChoice.slice(1)} vs. ${computerChoice.charAt(0).toUpperCase() +
        computerChoice.slice(1)}, You've LOST!</p>`;
    } else {
        result.innerHTML = `<p>Round ${scoreboard.round}, ${playerChoice.charAt(0).toUpperCase() +
        playerChoice.slice(1)} vs. ${computerChoice.charAt(0).toUpperCase() +
        computerChoice.slice(1)}, It's A Tie!</p>`;
    }

    if (scoreboard.player === 3 || scoreboard.computer === 3) {
        if (scoreboard.player === 3) {
            result.innerHTML = `
                                <p class="won">You've WON!</p>
                                <p class="score">${scoreboard.player}:${scoreboard.computer}</p>
                               `;
        } else {
            result.innerHTML = `
                                <p class="lost">You've LOST!</p>
                                <p class="score">${scoreboard.player}:${scoreboard.computer}</p>
                               `;
        }
        scoreboard.player = 0;
        scoreboard.computer = 0;
        scoreboard.round = 0;
    }
}

const restartGame = () => {
    scoreboard.player = 0;
    scoreboard.computer = 0;
    scoreboard.round = 0;
    result.innerHTML = '';
}

choices.forEach(choice => choice.addEventListener('click', play));
reset.addEventListener('click', restartGame);