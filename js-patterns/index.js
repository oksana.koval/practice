const MagazineState = {
    ReadyForPushNotification: 1,
    ReadyForApprove: 2,
    ReadyForPublish: 3,
    PublishInProgress: 4
};

class Magazine {
    constructor() {
        this.state = MagazineState.ReadyForPushNotification;
        this.articles = [];
        this.subscribers = [];

    }
}

class MagazineEmployee {
    constructor(name, type, magazine) {
        this.name = name;
        this.type = type;
        this.magazine = magazine;
    }

    addArticle(article) {
        if (this.type === 'manager') {
            console.log('You do not have permissions to do it.')
            return;
        }
        let articleObj = {type: this.type, article: article};
        this.magazine.articles.push(articleObj);
        if (this.magazine.articles.length === 5) {
            this.magazine.state = MagazineState.ReadyForApprove;
        }
    }

    publish() {
        switch (this.magazine.state) {
            case MagazineState.ReadyForPushNotification:
                console.log(`Hello ${this.name}. You can't publish. We are creating publications now.`);
                break;
            case MagazineState.ReadyForApprove:
                console.log(`Hello ${this.name}. You can't publish. We don't have a manager's approval.`);
                break;
            case MagazineState.ReadyForPublish:
                console.log(`Hello ${this.name}. You've recently published publications.`);
                this.magazine.state = MagazineState.PublishInProgress;
                for (let article of this.magazine.articles) {
                    this.magazine.subscribers.
                    filter(x => x.topic === article.type).
                    forEach(e => e.follower.
                    onUpdate(article.article));
                }
                break;
            case MagazineState.PublishInProgress:
                console.log(`Hello ${this.name}. While we are publishing we can't do any actions.`);
                setTimeout(() => {
                    this.magazine.state = MagazineState.ReadyForPushNotification, 60000
                });
                break;
            default:
                break;
        }
    }

    approve() {
        if (this.type !== 'manager') {
            console.log('You do not have permissions to do it.');
            return;
        }
        switch (this.magazine.state) {
            case MagazineState.ReadyForPushNotification:
                console.log(`Hello ${this.name}. You can't approve. We don't have enough of publications.`);
                break;
            case MagazineState.ReadyForApprove:
                console.log(`Hello ${this.name}. You've approved the changes.`);
                this.magazine.state = MagazineState.ReadyForPublish;
                break;
            case MagazineState.ReadyForPublish:
                console.log(`Hello ${this.name}. Publications have been already approved by you.`);
                break;
            case MagazineState.PublishInProgress:
                console.log(`Hello ${this.name}. While we are publishing we can't do any actions.`);
                break;
            default:
                break;
        }
    }
}

class Follower {
    constructor(name) {
        this.name = name;
    }

    subscribeTo(magazine, topic) {
        let subscribe = {follower: this, topic: topic};
        magazine.subscribers.push(subscribe);
    }

    unsubscribe(magazine, topic) {
        magazine.subscribers = magazine.subscribers.filter(x => x.follower !== this && x.topic !== topic);
    }

    onUpdate(data) {
        console.log(`${data} ${this.name}`);
    }
}
